//
//  ViewController.swift
//  Notificaciones
//
//  Created by Master Móviles on 17/3/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onLaunchNotificationClicked(_ sender: Any) {
        // content
        let content = UNMutableNotificationContent()
        content.title = "Notificación"
        content.subtitle = "Esto es una prueba"
        content.body = "Probando las notificaciones con imágenes"
        content.userInfo = ["Mensaje": "Mensaje de notificacion"]
        content.badge = 1
        
        // Content attachment
        if let attachment = UNNotificationAttachment.create(identifier: "prueba", image: UIImage(named: "gatito.png")!, options: nil) {
            content.attachments = [attachment]
        }
        
        // Actions
        let acceptAction = UNNotificationAction(identifier: "accept", title: "Aceptar", options: [])
        let declineAction = UNNotificationAction(identifier: "decline", title: "Otro día", options: [])
        let messageAction = UNTextInputNotificationAction(identifier: "message", title: "Mensaje", options: [])
        
        let category = UNNotificationCategory(identifier: "invitation", actions: [acceptAction, declineAction, messageAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        content.categoryIdentifier = "invitation"
        
        // Trigger
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        
        // Identifier
        let requestIdentifier = "peticionEjemplo"
        
        // Request
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) {
            error in
            
            print("Error \(error)")
        }
    }

}

